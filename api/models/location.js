const mongoose  = require('mongoose');

const locationSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    latitude: Number,
    longitude: Number
});

module.exports = mongoose.model('Location', locationSchema);