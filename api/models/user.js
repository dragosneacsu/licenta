const mongoose  = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    firstName : String,
    lastName : String,
    CNP : String,
    phone : String,
    email : String,
    password: String,
    locationId: String
});

module.exports = mongoose.model('User', userSchema);