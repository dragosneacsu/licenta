const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Location = require('../models/location');
router.get('/',(req,res,next) => {
    Location.find()
    .exec()
    .then( docs => {
        console.log(docs);
        //if( docs.length >=0 ){
            res.status(200).json(docs);
        //} else {
        //    res.status(404).json({
       //        message:" No entries found"
        //    })
        //}
       
    })
    .catch( err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

router.post('/',(req,res,next) => {
    
    const location = new Location({
        _id : new mongoose.Types.ObjectId(),
        latitude : req.body.latitude,
        longitude : req.body.longitude
    });
    location.save().then( result => {
        console.log(result);
        res.status(201).json({
            message: 'Handling POST requests to /locations',
            createdLocation: location
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
    
});

router.get('/:locationId', (req, res, next) => {
    const id = req.params.locationId;
    Location.findById(id)
    .exec()
    .then(doc => {
        console.log("From database", doc);
        if( doc ) {
        res.status(200).json(doc);
        } else {
            res.status(404).json({message:" Nu exista acest id locatie in bd!"});
        }
    })
    .catch( err => {
        console.log(err);
        res.status(500).json({error: err});
});
});

router.patch('/:locationId', (req, res, next) => {
    const id = req.params.locationId;
    const updateOps = {};
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }
    Location.update({ _id : id }, { $set: updateOps
    })
    .exec()
    .then(result => {
        console.log(result);
        res.status(200).json(result);
    })
    .catch( err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});


router.delete('/:locationId', (req, res, next) => {
    const id = req.params.locationId;
    Location.deleteOne({ _id : id })
    .exec()
    .then( result => {
        res.status(200).json(result);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error : err
        });
    });
 });

module.exports = router;